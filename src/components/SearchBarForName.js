import React from "react";

class SearchBarForName extends React.Component {

  state = { word: "" };
  submit = (event) => {
    event.preventDefault();
    let filterList = [];
    if (this.state.word) {
      filterList = this.props.jobs.filter((string) => {
        if (
          /* checking the entered string is a substring in job name */
          string.name.toString().toLowerCase().includes(this.state.word.toString().toLowerCase())
        ) {
          return true;
        } else {
          return false;
        }
      });
    } else {
      filterList = this.props.jobs;
    }
    this.props.onSearchBarForName(filterList);
  };

  render() {
    return (
      <div className="ui segment">
        <form onSubmit={this.submit} className="ui form">
          <div className="field">
            <label>What:</label>
            <input
              ref={this.textInput}
              type="text"
              value={this.state.word}
              onChange={(e) => this.setState({ word: e.target.value })}
              placeholder="Job title,keywords or company"
            />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBarForName;
