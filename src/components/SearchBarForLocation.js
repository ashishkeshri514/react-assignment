import React from "react";

class SearchBarForLocation extends React.Component {
  state = { word: "" };

  submit = (event) => {
    event.preventDefault();
    let filterList = [];
    if (this.state.word) {
      filterList = this.props.jobs.filter((string) => {
        if (
          /* checking the entered string is a substring in job city name */
          string.location.city.toString().toLowerCase().includes(this.state.word.toString().toLowerCase())
        ) {
          return true;
        } else {
          return false;
        }
      });
    } else {
      filterList = this.props.jobs;
    }

    this.props.onSearchBarForLocation(filterList);
  };

  render() {
    return (
      <div className="ui segment">
        <form onSubmit={this.submit} className="ui form">
          <div className="field">
            <label>Where:</label>
            <input
              type="text"
              value={this.state.word}
              onChange={(e) => this.setState({ word: e.target.value })}
              placeholder="Enter City"
            />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBarForLocation;
