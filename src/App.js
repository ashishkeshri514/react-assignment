import { Component } from "react";
import "./App.css";
import JobBriefList from "./components/JobBriefList";
import SearchBarForLocation from "./components/SearchBarForLocation";
import SearchBarForName from "./components/SearchBarForName";
import Jobs from "./jobs.json";

class App extends Component {
  state = { filterJobs: Jobs, jobs: Jobs };
  filterByName = (e) => {

    this.setState({ filterJobs: e });
  };
  filterByLocation = (e) => {

    this.setState({ filterJobs: e });
  };
  render() {
    return (
      <div className="App" style={{ padding: "3px" }}>

        {/*  <Demo /> */}
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <SearchBarForName
            jobs={Jobs}
            onSearchBarForName={this.filterByName}
          />
          <SearchBarForLocation
            jobs={Jobs}
            onSearchBarForLocation={this.filterByLocation}
          />
        </div>

        <JobBriefList jobList={this.state.filterJobs} />
      </div>
    );
  }
}

export default App;
